        <!-- start of hero -->
        <section class="hero-slider hero-style-1">
            <div class="swiper-container">
                <div class="slide-main-text">
                    <div class="container">
                        <div class="slide-title">
                            <h2>Angga & Yuni</h2>
                        </div>
                        <div class="wedding-date">
                            <span>25 November 2020</span>
                        </div>
                        <div class="clearfix"></div>
                        <!-- <div class="count-down-clock">
                            <div id="clock"></div>
                        </div> -->
                    </div>
                    <div class="pattern">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="<?php echo base_url();?>assets/images/couple/5.jpeg"></div>
                    </div>

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="<?php echo base_url();?>assets/images/couple/2.jpeg"></div>
                    </div>

                    <div class="swiper-slide">
                        <div class="slide-inner slide-bg-image" data-background="<?php echo base_url();?>assets/images/couple/7.jpeg"></div>
                    </div><!-- end swiper-slide -->
                </div>
                <!-- end swiper-wrapper -->

                <!-- swipper controls -->
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </section>
        <!-- end of hero slider -->


        <!-- start invitation-section -->
        <section class="invitation-section section-padding">
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="invitation-box">
                            <div class="left-vec"></div>
                            <div class="right-vec"></div>
                            <div class="inner">
                                <h2>Save the Date</h2>
                                <span>For the wedding of</span>
                                <h3>Angga & Yuni</h3>
                                <p>Rabu, 25 November 2020 <br> banjar dinas jro wargi, desa bungkulan, kec. sawan, kab. buleleng ( depan kampus stikes buleleng)</p>
                                <a href="https://maps.app.goo.gl/12pDMq6p2R9zAZaQ9" target="blank" ><img style="width:60px;" src="<?php echo base_url();?>assets/images/logo-google-maps.png"></img></a><br><br>
                                <h5>Anda, </h5>
                                <h4><?php echo str_replace("-"," ",$nama);?></h4>
                                <h5>Diundang Pada Jam :  </h5>
                                <h4>
                                <?php
                                if ($jam == 'a'){
                                    echo '10.00';
                                } else if($jam == 'b'){
                                    echo '12.00';
                                } else if($jam == 'c'){
                                    echo '14.00';
                                } else if($jam == 'd'){
                                    echo '16.00';
                                } else {
                                    echo '18.00';
                                }

                                ?>

                                 Wita</h4>
                                <p>Mohon konfirmasi kehadiran dengan menekan RSVP, mengingat situasi dan kondisi di masa Pandemi</p>
                                <a href="index.html#rsvp" class="theme-btn" id="scroll">RSVP now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end invitation-section -->

        <!-- start gallery-section -->
        <section class="gallery-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Our Video</h2>
                            <p>Moment yang terlalu indah untuk dilewatkan.</p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-xs-12">
                    <iframe src="https://drive.google.com/file/d/1smpTcYD9vU9h-5yEAG680Y7HYpx7UPL9/preview"></iframe>
                    </div>
                </div> <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end gallery-section -->
        

        <!-- start couple-section -->
        <section class="couple-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Kami yang berbahagia</h2>
                            <p>Pertemuan yang sederhana, sempurna karena Cinta</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="couple-area">
                            <div class="couple-row clearfix">
                                <div class="img-holder">
                                    <img src="<?php echo base_url();?>assets/images/couple/angga-square.png" alt>
                                </div>
                                <div class="details">
                                    <div class="inner">
                                        <h2>I Made Angga Adi Saputra</h2>
                                        <h4>Om Swastyastu, </h4>
                                        <p>Saya adalah anak Kedua dari pasangan Gede Oka dan Ni Luh Sudiratmini <br>banjar dinas jro wargi, desa bungkulan, kec. sawan, kab. buleleng ( depan kampus stikes buleleng).</p>
                                        <ul class="social-links">
                                            <li><a href="https://www.facebook.com/bondan.noviadark"><i class="ti-facebook"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="couple-row last-row clearfix">
                                <div class="details">
                                    <div class="inner">
                                        <h2>Nyoman Yuni Tresnawati</h2>
                                        <h4>Om Swastyastu,</h4>
                                        <p>Saya adalah anak Ketiga dari pasangan I Putu Gede Eka Putra dan Umi Kalsum <br> banjar lelangon, dauh puri kaja, denpasar utara.</p>
                                        <ul class="social-links">
                                            <li><a href="https://www.facebook.com/bondan.noviadark"><i class="ti-facebook"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-twitter-alt"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-linkedin"></i></a></li>
                                            <li><a href="index.html#"><i class="ti-pinterest"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="img-holder">
                                    <img src="<?php echo base_url();?>assets/images/couple/yuni-square.png" alt>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end couple-section -->


        <!-- start story-section -->
        <!-- <section class="story-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Cerita Kami</h2>
                            <p>Sudah 7 tahun kami menjalani cinta kasih penuh tangtangan dan akhirnya kamipun mewujudkanya...</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="story-block">
                            <ul>
                                <li>
                                    <div class="details">
                                        <h3>Pertama Bertemu</h3>
                                        <span class="date">20 Maret 2012</span>
                                        <p>Kami bertemu pertama kali ketika masa kuliah. Ketertarikan mulai terlihat saat kami mengikuti beberapa kegiatan Organisasi bersama sama. </p>
                                    </div>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-1.png" alt>
                                    </div>
                                </li>
                                <li>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-2.png" alt>
                                    </div>
                                    <div class="details">
                                        <h3>Menyatakan Cinta</h3>
                                        <span class="date">23 Maret 2013</span>
                                        <p>Saya menyatakan cinta di Pelabuhaan Buleleng (yang saat ini menjadi tempat Favourite kami) tepat sebelum saya berangkat mengikuti Praktik Kerja Industri.</p>
                                    </div>
                                </li>
                                <li>
                                    <div class="details">
                                        <h3>Melamar</h3>
                                        <span class="date">16 Agustus 2020</span>
                                        <p>Setelah melalui proses panjang akhirnya saya memberanikan diri untuk melamar dihadapan Dwi Sanista, Komang Samya, Permadi Kusuma, dan Rika Kasih</p>
                                    </div>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-3.png" alt>
                                    </div>
                                </li>
                                <li>
                                    <div class="img-holder">
                                        <img src="<?php echo base_url();?>assets/images/story/img-4.png" alt>
                                    </div>
                                    <div class="details">
                                        <h3>Pernikahan</h3>
                                        <span class="date">25 November 2020</span>
                                        <p>Akhirnya Resepsi penikahan kami akan digelar di tanggal ini. Haru dan Bahagia akan bercampur dan kami ingin anda menjadi saksinya.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div> 
        </section> -->
        <!-- end story-section -->


        <!-- start gallery-section -->
        <section class="gallery-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Captured Moments</h2>
                            <p>Kami tengah berbahagia, kami berharap anda ikut merasakanya. </p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col col-xs-12">
                        <div class="gallery-container gallery-fancybox masonry-gallery">
                            <div class="grid grid-item">
                                <a href="<?php echo base_url();?>assets/images/couple/3.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/3.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item">
                                <a href="<?php echo base_url();?>assets/images/couple/4.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/4.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/5.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/5.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/6.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/6.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/8.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/8.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/9.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/9.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/12.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/12.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/13.jpeg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/13.jpeg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/casual3.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/casual3.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/casual4.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/casual4.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <div class="grid grid-item width2">
                                <a href="<?php echo base_url();?>assets/images/couple/casual5.jpg" class="fancybox" data-fancybox-group="gall-1">
                                    <img src="<?php echo base_url();?>assets/images/couple/casual5.jpg" alt class="img img-responsive">
                                </a>
                            </div>
                            <!-- <div class="grid grid-item width2">
                                <a href="https://www.youtube.com/embed/XSGBVzeBUbk?autoplay=1" data-type="iframe" class="video-play-btn">
                                    <img src="<?php //echo base_url();?>assets/images/gallery/img-6.jpg" alt class="img img-responsive">
                                    <i class="ti-control-play"></i>
                                </a>
                            </div> -->
                        </div>
                    </div>
                </div> <!-- end row -->

            </div> <!-- end container -->
        </section>
        <!-- end gallery-section -->


        <!-- start contact-section -->
        <section class="contact-section section-padding p-t-0" id="rsvp">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <div class="section-title">
                            <h2>Apakah anda akan hadir?</h2>
                            <p>Jika berkenan, mohon untuk mengisi data diri dan tekan "Konfirmasi" agar kami dapat menyiapkan protokol kesehatan dengan maksimal.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-lg-10 col-lg-offset-1">
                        <div class="contact-form">
                            <?php echo form_open_multipart('home/send	', 'class="form validate-rsvp-form row"'); ?>
                                <div class="col col-sm-6">
                                    <input type="text" name="name" class="form-control" placeholder="Nama Anda*">
                                </div>
                                <div class="col col-sm-6">
                                    <input type="email" name="email" class="form-control" placeholder="Email Anda*">
                                </div>
                                <div class="col col-sm-6">
                                    <select class="form-control" name="guest" >
                                        <option disabled selected>Jumlah Tamu*</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                    </select>
                                </div>
                                <div class="col col-sm-6">
                                    <select class="form-control" name="atentand" >
                                        <option>Saya Akan Hadir*</option>
                                        <option>Saya Mungkin Berhalangan</option>
                                        <option>Saya Berhalangan</option>
                                    </select>
                                </div>
                                <div class="col col-sm-12">
                                    <textarea class="form-control" name="notes" placeholder="Pesan dan Saran anda*"></textarea>
                                </div>
                                <div class="col col-sm-12 submit-btn">
                                    <button type="submit" class="theme-btn">Konfirmasi</button>
                                    <div id="loader">
                                        <i class="ti-reload"></i>
                                    </div>
                                </div>
                                <div class="col col-md-12 success-error-message">
                                    <div id="success">Thank you</div>
                                    <div id="error"> Error occurred while sending email. Please try again later. </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end contact-section -->


        <!-- start event-section -->
        <section class="event-section section-padding p-t-0">
            <div class="top-area">
                <h2>Celebrate Our Love</h2>
                <p class="date">25 November 2020</p>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col col-xs-12">
                        <div class="event-grids clearfix">
                            
                        </div>
                    </div>
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end event-section -->


        <!-- start partners-section -->
        <section class="partners-section section-padding p-t-0">
            <div class="container">
                <div class="row">
                    <!-- <div class="col col-xs-12">
                        <h2>Gift Registry</h2>
                        <div class="partner-grids clearfix">
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-1.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-2.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-3.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-4.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-5.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-6.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-7.jpg" alt>
                            </div>
                            <div class="grid">
                                <img src="<?php //echo base_url();?>assets/images/partners/img-8.jpg" alt>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div> <!-- end container -->
        </section>
        <!-- end partners-section -->
