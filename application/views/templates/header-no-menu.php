<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="irstheme">

    <title>Angga & Yuni </title>
    
    <link href="<?php echo base_url();?>assets/css/themify-icons.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/flaticon.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/swiper.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery.fancybox.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/anggayuni.css" rel="stylesheet">
    <link rel="icon" type="image/png" href="<?php echo base_url();?>assets/images/fav.png">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- start page-wrapper -->
    <div class="page-wrapper flower-fixed-body">

        <!-- start preloader -->
        <div class="preloader">
            <div class="middle">
                <i class="fi flaticon-favorite-heart-button"></i>
                <i class="fi flaticon-favorite-heart-button"></i>
                <i class="fi flaticon-favorite-heart-button"></i>
                <i class="fi flaticon-favorite-heart-button"></i>
            </div>
        </div>
        <!-- end preloader --> 

        <!-- Start header -->
        <header id="header" class="site-header header-style-1">
            <div class="topbar">
                <div class="container">
                    <div class="row">
                        <div class="col col-xs-12">
                            <div class="site-logo">
                                <a href="index.html">
                                    <h1>Angga & Yuni</h1>
                                    <span>Will be Married</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- end topbar -->
            <!-- put menu navbar here-->
        </header>
        <!-- end of header -->